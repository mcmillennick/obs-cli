package main

import (
	obsws "github.com/muesli/go-obs-websocket"
	"github.com/spf13/cobra"
)

var (
	toggleRecordingCmd = &cobra.Command{
		Use:   "toggle-recording",
		Short: "Stop/Stop recording toggle",
		RunE: func(cmd *cobra.Command, args []string) error {
			return toggleRecording()
		},
	}
	startRecordingCmd = &cobra.Command{
		Use:   "start-recording",
		Short: "Start recording",
		RunE: func(cmd *cobra.Command, args []string) error {
			return startRecording()
		},
	}
	stopRecordingCmd = &cobra.Command{
		Use:   "stop-recording",
		Short: "Stop recording",
		RunE: func(cmd *cobra.Command, args []string) error {
			return stopRecording()
		},
	}
)

func toggleRecording() error {
	req := obsws.NewStartStopRecordingRequest()
	return req.Send(*client)
}

func startRecording() error {
	req := obsws.NewStartRecordingRequest()
	return req.Send(*client)
}

func stopRecording() error {
	req := obsws.NewStopRecordingRequest()
	return req.Send(*client)
}

func init() {
	rootCmd.AddCommand(toggleRecordingCmd)
	rootCmd.AddCommand(stopRecordingCmd)
	rootCmd.AddCommand(startRecordingCmd)
}
