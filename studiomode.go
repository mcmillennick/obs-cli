package main

import (
	"errors"

	obsws "github.com/muesli/go-obs-websocket"
	"github.com/spf13/cobra"
)

var (
	transitionToProgramCmd = &cobra.Command{
		Use:   "transition-to-program",
		Short: "Set preview to program in studio mode",
		RunE: func(cmd *cobra.Command, args []string) error {
			// if len(args) < 1 {
			// 	return errors.New("switch-scene requires a scene name as argument")
			// }
			return transitionToProgram("name")
		},
	}
	setPreviewSceneCmd = &cobra.Command{
		Use:   "set-preview-scene",
		Short: "Set the preview scene in studio mode",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("set-preview requires a scene name as argument")
			}
			return setPreviewScene(args[0])
		},
	}
)

func transitionToProgram(scene string) error {
	m := make(map[string]interface{})
	req := obsws.NewTransitionToProgramRequest(m, "move", 500)
	return req.Send(*client)
}

func setPreviewScene(scene string) error {
	req := obsws.NewSetPreviewSceneRequest(scene)
	return req.Send(*client)
}

func init() {
	rootCmd.AddCommand(setPreviewSceneCmd)
	rootCmd.AddCommand(transitionToProgramCmd)
}
